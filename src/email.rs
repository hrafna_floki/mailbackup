use std::str::Utf8Error;
use std::fmt::{Display, Formatter, Debug};
use std::fs::File;
use std::io::{Error, ErrorKind, Write};
use std::path::Path;

pub struct EMail {
    pub id: String,
    pub raw: Vec<u8>
}

impl EMail {

    pub fn new(id: String, raw: Vec<u8>) -> Self {
        EMail { id, raw }
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn to_string(&self) -> Result<String, Utf8Error> {
        let parsed = std::str::from_utf8(&*self.raw)?;
        Ok(parsed.to_string())
    }

    pub fn write_to_file(&self, out_dir: &Path, overwrite: &bool) -> Result<(), Error> {
        let out_path = out_dir.join(format!("{}.eml", self.get_id()));
        if !out_path.exists() || *overwrite {
            let mut file = File::create(&out_path)?;
            return file.write_all(&self.raw);
        } else {
            Err(Error::new(ErrorKind::AlreadyExists,
                           format!("{}.eml already exists.", self.get_id())))
        }
    }
}

impl Display for EMail {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let res = match self.to_string() {
            Ok(res) => {res }
            Err(_) => { String::from("NA")}
        };
        write!(f, "{}", res)
    }
}

impl Debug for EMail {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let res = match self.to_string() {
            Ok(res) => {res }
            Err(_) => { String::from("NA")}
        };
        write!(f, "{}", res)
    }
}
