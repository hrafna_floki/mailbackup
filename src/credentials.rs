use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Credentials {
    pub hostname: String,
    pub port: u16,
    pub username: String,
    pub password: String
}

impl Credentials {
    /// Creates a new Credentials object
    /// # Arguments
    /// * `hostname` - The hostname
    /// * `port` - The port (optional, default: 993)
    /// * `username` - The username
    /// * `password` - The password
    /// # Examples
    /// ```
    /// use mailbackup::credentials::Credentials;
    /// let creds = Credentials::new(
    ///     String::from("imap.gmail.com"),
    ///     None,
    ///     String::from("username"),
    ///     String::from("password"));
    /// ```
    pub fn new(hostname: String, port: Option<u16>, username: String, password: String) -> Self {
        Credentials {
            hostname,
            port: port.unwrap_or(993),
            username,
            password
        }
    }
}
