use std::fs;
use std::fs::create_dir_all;
use std::net::TcpStream;
use std::path::{Path, PathBuf};
use mailbackup::credentials::Credentials;
use mailbackup::utils::{connect, create_folder, export_folder, list_folders, open_folder, read_email_from_file, save_email};
use walkdir::WalkDir;
use clap::{Parser, Subcommand};
use imap::Session;
use native_tls::TlsStream;

/// Export/Import EMails from an IMAP account to/from local disk
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Args {

    #[command(subcommand)]
    command: Commands,

    #[arg(short, long)]
    /// Path to the JSON file holding the credentials
    creds: String,

    #[arg(short, long)]
    /// Path to the directory where the EMails will be exported to, respectively imported from
    dir: String,

    #[arg(short, long, default_value_t = false)]
    /// Traverse into sub-folders/directories (default: false)
    recursive: bool,

    #[arg(short, long, default_value_t = String::from("INBOX"))]
    /// Name of the mailbox (i. e. "folder") to export. Note: If you omit this the default "INBOX"
    /// will be used, which usually is the top-level folder
    mailbox: String
}

#[derive(Subcommand)]
enum Commands {
    /// Download EMails from IMAP server and save in local directory
    Pull {
        #[arg(short, long, default_value_t = false)]
        /// Flag to indicate to overwrite already existing EMails (default: false)
        overwrite: bool,
    },

    /// Upload EMails from local directory to the IMAP server. Note: There's no check if the
    /// EMails already exist. If you pull and push, you'll duplicate the EMails.
    Push { }
}


fn handle_entry(session: &mut Session<TlsStream<TcpStream>>, args: &Args, path: &Path)
    -> Result<(), String> {
    let dir = Path::new(&args.dir);
    let rel = path.strip_prefix(dir).unwrap();
    if path.is_dir() {
        let folder = rel.display().to_string();
        // prefix with specified mailbox name INBOX/... to get the 'absolute' path
        let mb_path = match folder.is_empty() {
            true => { String::from(&args.mailbox) }
            false => { format!("{}/{}", &args.mailbox, folder) }
        };
        if create_folder(session, &mb_path).is_err() {
            return Err(String::from(format!("Could not create folder {}", mb_path)));
        } else {
            println!("Created folder : {}", mb_path);
        }
    } else if rel.extension().unwrap() == "eml" {
        let folder = String::from(rel.parent().unwrap().to_str().unwrap());
        // prefix with specified mailbox name INBOX/... to get the 'absolute' path
        let mb_path = match folder.is_empty() {
            true => { String::from(&args.mailbox) }
            false => { format!("{}/{}", &args.mailbox, folder) }
        };
        match open_folder(session, &mb_path, false) {
            Ok(_) => {}
            Err(err) => {
                return Err(String::from(format!("Could not open folder {}: {}", mb_path, err)));
            }
        };
        let email = read_email_from_file(path).unwrap();
        if save_email(session, &mb_path, email.as_bytes()).is_err() {
            return Err(String::from(format!("Could not upload email {}", path.display().to_string())));
        }
    }
    return Ok(());
}

fn main() {

    let args = Args::parse();

    match &args.command {

        Commands::Pull { overwrite } => {
            let out_dir = Path::new(&args.dir);

            let cred_file = fs::read_to_string(&args.creds).
                expect("Could not open credentials file.");

            let creds: Credentials = serde_json::from_str(cred_file.as_str()).
                expect("Could not interpret content of credentials file.");

            let mut session = connect(&creds).expect("Could not connect");

            let folders = match &args.recursive {
                true => {
                    list_folders(&mut session, &args.mailbox)
                    .expect("Could not list folders.")
                }
                false => { vec![String::from(&args.mailbox)] }
            };

            for folder in folders {
                println!("Processing {}", folder);
                // remove the specified mailbox name INBOX/... from the 'absolute' folder path
                let mut rel_mb_path = folder.strip_prefix(&args.mailbox).unwrap_or("");
                if rel_mb_path.starts_with("/") {
                    rel_mb_path = rel_mb_path.strip_prefix("/").unwrap();
                }

                let mut out_path = PathBuf::from(&out_dir);
                if !rel_mb_path.is_empty() {
                    out_path.push(&rel_mb_path);
                    create_dir_all(&out_path).expect("Can't create export directory");
                }
                export_folder(&mut session, &folder, &out_path, overwrite);
            }

            session.close().expect("Couldn't close session.");
        }

        Commands::Push { } => {
            let dir = Path::new(&args.dir);

            let cred_file = fs::read_to_string(&args.creds).
                expect("Could not open credentials file.");

            let creds: Credentials = serde_json::from_str(cred_file.as_str()).
                expect("Could not interpret content of credentials file.");

            let mut session = connect(&creds).expect("Could not connect");

            let mb_path= String::from(&args.mailbox);
            if create_folder(&mut session, &mb_path).is_err() {
                panic!("Specified mailbox doesn't exist and can't be created.");
            } else {
                println!("Created folder : {}", mb_path);
            }

            let d = match &args.recursive {
                true => { 100 },
                false => { 1 }
            };

            for entry in WalkDir::new(dir).max_depth(d).into_iter()
                .filter_map(|e| e.ok()) {
                let path : &Path = entry.path();
                if d == 1 && path.is_dir() {
                    continue;
                }
                match handle_entry(&mut session, &args, path) {
                    Ok(_) => { println!("Uploaded email {}", path.display().to_string()); }
                    Err(e) => { eprintln!("{}", e); }
                }
            }

            session.close().expect("Couldn't close session.");
        }
    }
}