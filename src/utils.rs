use std::fs::File;
use std::io::{BufReader, Read};
use imap::{Error, Session};
use native_tls::TlsStream;
use std::net::TcpStream;
use std::path::Path;
use imap::types::Flag;

use crate::credentials::Credentials;
use crate::email::EMail;

pub fn connect(creds :&Credentials)
    -> Result<Session<TlsStream<TcpStream>>, Error> {
    let tls = native_tls::TlsConnector::builder().build().unwrap();

    let client = imap::connect((creds.hostname.as_str(), creds.port),
                               creds.hostname.as_str(), &tls).unwrap();

    client.login(&creds.username, &creds.password)
        .map_err(|e| e.0)
}

// I don't think this works:
// pub fn get_separator(session: &mut Session<TlsStream<TcpStream>>) -> Result<String, String> {
//     let sep = match session.list(None, None) {
//         Ok( sep ) => { sep }
//         Err(err) => {
//             return Err(String::from(format!("Could not get separator: {}", err)));
//         }
//     };
//     let sep_string = match sep.len() {
//         1 => { let tmp = sep.get(0).unwrap();
//             String::from(tmp.name())
//         }
//         _ =>  {
//             return Err(String::from("Could not get separator."));
//         }
//     };
//     Ok(sep_string)
// }

pub fn list_folders(session: &mut Session<TlsStream<TcpStream>>, root: &String)
    -> imap::error::Result<Vec<String>> {
    let mb_name = format!("{}/*", root);
    let folders = session.list(Some(""),
                               Some(mb_name.as_str()))?;
    let mut result = Vec::new();
    result.push(root.clone());
    for folder in folders.iter() {
        result.push(String::from(folder.name()));
    }
    Ok(result)
}

/// Opens an IMAP mailbox (folder)
/// # Arguments
/// * `session` - Reference to the IMAP session
/// * `folder` - Name (resp. path) of the folder to open
/// * `read_only` - Open the mailbox in read-only mode
/// # Returns
/// The number of emails in this mailbox.
/// # Errors
// If the mailbox can't be opened.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use std::vec;
/// use mailbackup::credentials::Credentials;
/// use mailbackup::utils::{connect, open_folder};
/// let creds = Credentials::new(String::from("imap.gmail.com"), None, String::from("username"),
///     String::from("password"));
/// let mut session = connect(&creds).expect("Could not connect");
/// let n_emails = open_folder(&mut session, &String::from("INBOX/Test"), true).unwrap();
/// ```
pub fn open_folder(session: &mut Session<TlsStream<TcpStream>>, folder : &String, read_only: bool)
    -> Result<u32, Error> {
    let status = match read_only {
        true => { session.examine(folder.as_str())? }
        false => { session.select(folder.as_str())? }
    };
    Ok(status.exists)
}

/// Creates an IMAP mailbox (folder) if it doesn't already exist.
/// # Arguments
/// * `session` - Reference to the IMAP session
/// * `folder` - Name (resp. path) of the folder to create
/// # Errors
/// If the mailbox can't be created. If the mailbox already exists will NOT error.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use std::vec;
/// use mailbackup::credentials::Credentials;
/// use mailbackup::utils::{connect, create_folder};
/// let creds = Credentials::new(String::from("imap.gmail.com"), None, String::from("username"),
///     String::from("password"));
/// let mut session = connect(&creds).expect("Could not connect");
/// create_folder(&mut session, &String::from("INBOX/Test"));
/// ```
pub fn create_folder(session: &mut Session<TlsStream<TcpStream>>, folder : &String) -> Result<(), Error> {
    match session.examine(folder) {
        Ok(_res) => {
            return Ok(());
        }
        Err(_) => {}
    }
    session.create(folder)
}

/// Simply reads a file and returns its content as UTF8 String
/// # Arguments
/// * `path` - The path to the file
/// # Returns
/// The file content as UTF8 String if the can be parsed as UTF8
/// # Errors
/// If the file can't be read or parsed as UTF8.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use mailbackup::utils::read_email_from_file;
/// let email_string = read_email_from_file(Path::new("/home/user/test.eml")).unwrap().as_bytes();
/// ```
pub fn read_email_from_file(path: &Path) -> Result<String, String> {
    let f = match File::open(path) {
        Ok(f) => { f }
        Err(_) => { return Err(String::from("Could not open file.")); }
    };
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();
    match reader.read_to_end(&mut buffer){
        Ok(_) => {}
        Err(_) => { return Err(String::from("Could not read file.")); }
    };

    match std::str::from_utf8(&*buffer) {
        Ok(parsed) => { return Ok(String::from(parsed)); }
        Err(_) => { return Err(String::from("Could not parse email.")); }
    };
}

/// Saves an EMail into a specific IMAP mailbox (folder)
/// # Arguments
/// * `session` - Reference to the IMAP session
/// * `folder` - The mailbox (folder)
/// * `email` - The email as UTF8 byte array
/// # Errors
/// If the email can't be stored.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use std::vec;
/// use mailbackup::credentials::Credentials;
/// use mailbackup::utils::{connect, get_email, read_email_from_file, save_email};
/// let creds = Credentials::new(String::from("imap.gmail.com"), None, String::from("username"),
///     String::from("password"));
/// let mut session = connect(&creds).expect("Could not connect");
/// let email = read_email_from_file(Path::new("/home/user/test.eml")).unwrap().as_bytes();
/// save_email(&mut session, &String::from("INBOX/Test"), &email);
/// ```
pub fn save_email(session: &mut Session<TlsStream<TcpStream>>,
                  folder : &String, email: &[u8]) -> imap::Result<()> {
    let flags = [Flag::Seen];
    session.append_with_flags(folder, email, &flags)
}

/// Fetch a specific EMail
/// # Arguments
/// * `session` - Reference to the IMAP session
/// * `n` - The index of the email
/// # Returns
/// The EMail if it can be fetched.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use mailbackup::credentials::Credentials;
/// use mailbackup::utils::{connect, get_email};
/// let creds = Credentials::new(String::from("imap.gmail.com"), None, String::from("username"),
///     String::from("password"));
/// let mut session = connect(&creds).expect("Could not connect");
/// let email = get_email(&mut session, 1).unwrap();
/// ```
pub fn get_email(session: &mut Session<TlsStream<TcpStream>>, n: u32)
    -> Option<EMail> {
    let tmp = match session.fetch(format!("{}", n), "RFC822") {
        Ok(res) => { res }
        Err(err) => {
            eprintln!("Could not fetch email. {:?}", err);
            return None;
        }
    };
    let msg = match tmp.iter().next() {
        None => {return None;}
        Some(msg) => {msg}
    };
    let raw = match msg.body() {
        None => {
            eprintln!("Message did not have a body.");
            return None;
        }
        Some(raw) => { raw}
    };
    let id = match msg.uid {
        None => {
            format!("{}", crc32fast::hash(raw))
        }
        Some(id) => { id.to_string() }
    };
    Some(EMail::new(id, raw.to_vec()))
}

/// Fetches all emails of specified mailbox and saves them as *.eml into the
/// given output path
/// # Arguments
/// * `session` - Reference to the IMAP session
/// * `folder` - Folder (resp. mailbox) on the IMAP server to export
/// * `overwrite` - Flag to overwrite existing *.eml files.
/// # Examples
/// ```no_run
/// use std::path::Path;
/// use mailbackup::credentials::Credentials;
/// use mailbackup::utils::{connect, export_folder};
/// let creds = Credentials::new(String::from("imap.gmail.com"), None, String::from("username"),
///     String::from("password"));
/// let mut session = connect(&creds).expect("Could not connect");
/// export_folder(&mut session, &String::from("INBOX/Test"), Path::new("/home/user/export"), &false);
/// ```
pub fn export_folder(session: &mut Session<TlsStream<TcpStream>>, folder: &String, path: &Path, overwrite: &bool) {
    let n = match open_folder(session, folder, true) {
        Ok(n) => {n}
        Err(err) => {
            eprintln!("Could not open folder. {:?}", err);
        return;
        }
    };

    let mut i = 0;
    while i < n {
        i += 1;
        let email = match get_email(session, i) {
            Some(email) => { email }
            None => {
                eprintln!("Could not get email.");
                continue;
            }
        };
        match email.write_to_file(path, overwrite) {
            Ok(_) => { println!("Saved EMail {}.eml.", email.get_id()); }
            Err(err) => { eprintln!("Could not save EMail: {}", err); }
        }
    }
}